package com.example.smartlearning.ui.add

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import java.time.LocalDateTime

val DATABASENAME = "MY DATABASE"
val TABLENAME = "Cards"
val COL_QUESTION = "Question"
val COL_ANSWER = "Answer"
val COL_ID = "id"
val COL_LASTANSWERMOMENT= "LastAnswerMoment"
val COL_QUESTION_NUMBER = "number"

class DataBaseHandler(var context: Context) : SQLiteOpenHelper(context, DATABASENAME, null,
    1) {

    override fun onCreate(db: SQLiteDatabase?) {
        val createTable =
            "CREATE TABLE $TABLENAME ($COL_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "$COL_QUESTION VARCHAR(256), $COL_ANSWER VARCHAR(256)," +
                    "$COL_LASTANSWERMOMENT BIGINT, $COL_QUESTION_NUMBER INTEGER)"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //db?.execSQL(createTable)
    }

    fun myUpdate(card: Card) {
        val deleteCard = "DELETE FROM $TABLENAME WHERE $COL_ID = " + card.id
        val database = this.writableDatabase
        database.execSQL(deleteCard)
        insertData(card, card.id)
    }

    fun insertData(card: Card, id: Int = -1) {
        val database = this.writableDatabase    
        val contentValues = ContentValues()
        contentValues.put(COL_QUESTION, card.question)
        contentValues.put(COL_ANSWER, card.answer)
        contentValues.put(COL_LASTANSWERMOMENT, card.last_answer_time)
        contentValues.put(COL_QUESTION_NUMBER, card.number)
        if (id != -1)
            contentValues.put(COL_ID, card.id)
        val result = database.insert(TABLENAME, null, contentValues)
//        if (result == (0).toLong()) {
//            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
//        }
//        else {
//            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
//        }
    }

    fun readData(): MutableList<Card> {
        val list: MutableList<Card> = ArrayList()
        val db = this.readableDatabase
        val query = "Select * from $TABLENAME"
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                val card = Card()
                card.id = result.getString(result.getColumnIndex(COL_ID)).toInt()
                card.question = result.getString(result.getColumnIndex(COL_QUESTION))
                card.answer = result.getString(result.getColumnIndex(COL_ANSWER))
                card.number = result.getString(result.getColumnIndex(COL_QUESTION_NUMBER)).toInt()
                card.last_answer_time = result.getString(result.getColumnIndex(COL_LASTANSWERMOMENT)).toLong()
                list.add(card)
            }
            while (result.moveToNext())
        }
        result.close()
        return list
    }
}