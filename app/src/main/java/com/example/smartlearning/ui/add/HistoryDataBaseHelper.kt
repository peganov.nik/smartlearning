package com.example.smartlearning.ui.add

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

val HISTORYDATABASENAME = "HISTORY DATABASE"
val HISTORYTABLENAME = "History_Cards"
val COL_NUMBER = "number"
val COL_TIME = "time"
val COL_TYPE = "type"

class HistoryDataBaseHandler(var context: Context) : SQLiteOpenHelper(context, HISTORYDATABASENAME, null,
        1) {

    override fun onCreate(db: SQLiteDatabase?) {
        val createTable =
                "CREATE TABLE $HISTORYTABLENAME ($COL_NUMBER INTEGER, $COL_TIME BIGINT, $COL_TYPE VARCHAR(256))"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //onCreate(db);
    }

    fun insertData(action: Action) {
        val database = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COL_TIME, action.time)
        contentValues.put(COL_TYPE, action.type)
        val result = database.insert(HISTORYTABLENAME, null, contentValues)
//        if (result == (0).toLong()) {
//            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
//        }
//        else {
//            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
//        }
    }

    fun readData(number: Int): MutableList<Action> {
        val list: MutableList<Action> = ArrayList()
        val db = this.readableDatabase
        val query = "Select * from $HISTORYTABLENAME WHERE $COL_NUMBER = $number ORDER BY $COL_TIME"
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                val action = Action()
                action.type = result.getString(result.getColumnIndex(COL_TYPE))
                action.number = result.getString(result.getColumnIndex(COL_NUMBER)).toInt()
                action.time = result.getString(result.getColumnIndex(COL_TIME)).toLong()
                list.add(action)
            }
            while (result.moveToNext())
        }
        result.close()
        return list
    }
}