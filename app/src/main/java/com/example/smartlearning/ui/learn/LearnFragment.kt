package com.example.smartlearning.ui.learn

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.smartlearning.R
import com.example.smartlearning.ui.add.Action
import com.example.smartlearning.ui.add.Card
import com.example.smartlearning.ui.add.DataBaseHandler
import com.example.smartlearning.ui.add.HistoryDataBaseHandler
import kotlin.math.*

class LearnFragment : Fragment() {

    private lateinit var learnViewModel: LearnViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        learnViewModel =
                ViewModelProvider(this).get(LearnViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_learn, container, false)
        val textView: TextView = root.findViewById(R.id.text_learn)
        val wrongButton = root.findViewById<Button>(R.id.wrong_button)
        val correctButton = root.findViewById<Button>(R.id.correct_button)
        val db = DataBaseHandler(requireActivity())
        val adb = HistoryDataBaseHandler(requireActivity())

        var cardType = "Start"
        val cards = db.readData().toTypedArray()

        cards.sortWith(compareBy {it.card_time(adb)})

        learnViewModel.text.observe(viewLifecycleOwner, Observer {
            if (cards.size > 0)
            {
                if (cards[0].card_time(adb) <= System.currentTimeMillis()) {
                    textView.text = cards[0].question
                    cardType = "Question"
                }
                else {
                    textView.text = "All cards have been learned!"
                    cardType = "Start"
                }
                textView.bottom
            } else
            {
                textView.text = "Add your first card!"
                textView.bottom
            }
        })

        root.setOnClickListener {
            if (cardType == "Question") {
                cardType = "Answer"
                textView.text = cards[0].answer
                wrongButton.visibility = Button.VISIBLE
                correctButton.visibility = Button.VISIBLE
            }
        }


        wrongButton.setOnClickListener {
            adb.insertData(Action("<", cards[0].number, System.currentTimeMillis()))
            cards[0].number = 0
            cards[0].last_answer_time = System.currentTimeMillis()
            db.myUpdate(cards[0])
            cards.sortWith(compareBy {it.card_time(adb)})

            if (cards[0].card_time(adb) <= System.currentTimeMillis()) {
                textView.text = cards[0].question
                cardType= "Question"
            }
            else {
                textView.text = "All cards have been learned!"
            }
            wrongButton.visibility = Button.INVISIBLE
            correctButton.visibility = Button.INVISIBLE
        }

        correctButton.setOnClickListener {
            adb.insertData(Action(">", cards[0].number, System.currentTimeMillis()))
            cards[0].number++
            cards[0].last_answer_time = System.currentTimeMillis()
            db.myUpdate(cards[0])
            cards.sortWith(compareBy {it.card_time(adb)})

            if (cards[0].card_time(adb) <= System.currentTimeMillis()) {
                textView.text = cards[0].question
                cardType = "Question"
            }
            else {
                textView.text = "All cards have been learned!"
            }
            wrongButton.visibility = Button.INVISIBLE
            correctButton.visibility = Button.INVISIBLE
        }

        return root
    }
}

