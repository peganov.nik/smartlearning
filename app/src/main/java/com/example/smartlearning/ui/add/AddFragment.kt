package com.example.smartlearning.ui.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.smartlearning.R

class AddFragment : Fragment() {

    private lateinit var addViewModel: AddViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        addViewModel =
                ViewModelProvider(this).get(AddViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_add, container, false)
        val AddButton: Button = root.findViewById(R.id.AddButton)
        val ReadButton: Button = root.findViewById(R.id.ReadButton)
        val addQuestion: TextView = root.findViewById(R.id.addQuestion)
        val addAnswer: TextView = root.findViewById(R.id.addAnswer)
        val tvResult: TextView = root.findViewById(R.id.tvResult)
        val db = DataBaseHandler(requireActivity())
        val adb = HistoryDataBaseHandler(requireActivity())

        AddButton.setOnClickListener {
            var card = Card(addQuestion.text.toString(), addAnswer.text.toString())
            card.last_answer_time = System.currentTimeMillis()
            db.insertData(card)
        }

        ReadButton.setOnClickListener{
            val data = db.readData()
            tvResult.text = ""
            for (i in 0 until data.size) {
                tvResult.append(
                    data[i].id.toString() + " " + data[i].question + " " + data[i].answer + " " +
                            data[i].number + " " + data[i].last_answer_time + " " +
                            data[i].card_time(adb) + "\n"
                )
            }
        }
        return root
    }
}