package com.example.smartlearning.ui.add
import kotlin.math.pow

class Card(var question: String = "", var answer: String = "") {
    var id: Int = 0
    var number: Int = 0
    var last_answer_time: Long = 0

    fun card_time(adb: HistoryDataBaseHandler): Long {
        var actions = adb.readData(this.number).toMutableList()
        if (actions.size == 0) {
            adb.insertData(Action(">", number, System.currentTimeMillis()))
            return last_answer_time +  (1000 * (2.0).pow(number.toDouble())).toLong()
        }
        var more_count: List<Int> = emptyList<Int>()
        var previous = 0
        for (action in actions) {
            if (action.type == ">")
                more_count += previous + 1
            else
                more_count += previous
            previous = more_count[-1]
        }
        var less_count: List<Int> = emptyList<Int>()
        previous = 0
        for (action in actions.asReversed()) {
            if (action.type == "<")
                less_count += previous + 1
            else
                less_count += previous
            previous = less_count[-1]
        }
        var mx = -1
        var index = -1
        for (i in 0..actions.size) {
            var new_value = more_count[i] + less_count[i + 1]
            if (mx == -1 || new_value > mx) {
                mx = new_value
                index = i
            }
        }
        if (index + 1 < actions.size)
            return last_answer_time + (actions[index].time + actions[index + 1].time) / 2
        return last_answer_time + actions[-1].time * 2
    }
}


